#lang sicp

(rule (append-to-form () ?y ?y))
(rule (append-to-form (?u . ?v) ?y (?u . ?z))
      (append-to-form ?v ?y ?z))

(rule (reverse (?r.()) (?r.())))

(rule (reverse ?r ?l)
      (and (append-to-form ?x1 ?y1 ?l)
           (append-to-form ?x2 ?y2 ?r)
           (reverse ?x1 ?y2)
           (reverse ?y1 ?x2)))





