#lang sicp

(rule ((grandson) ?x ?y)
      (grandson ?x ?y))

(rule (end-with-grandson (grandson)))

(rule (end-with-grandson (?x.?rel))
      (end-with-grandson ?rel))

(rule (great. ?rel ?x ?y)
      (and (son ?f ?y)
           (end-with-grandson ?rel)
           (?rel ?x ?y)))


