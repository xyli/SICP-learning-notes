#lang planet neil/sicp

(define (cont-frac n d k)
  (define (iter i res)
    (if (= i 0) res
        (iter (- i 1) (/ (n i) (+ (d i) res)))))
  (iter (- k 1) (/ (n k) (d k))))

(define (d-seq i)
  (if (= (remainder i 3) 2)
      (* 2 (/ (+ i 1) 3))
      1))

(define e (+ (cont-frac (lambda (i) 1.0) d-seq 10) 2))
