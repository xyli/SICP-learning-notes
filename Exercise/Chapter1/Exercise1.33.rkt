#lang planet neil/sicp


(define (filtered-accumulate filter combiner null-value term a next b)
  (if (> a b)
      null-value
      (if (filter a)
          (combiner (term a)
                    (filtered-accumulate filter combiner null-value term (next a) next b))
          (filtered-accumulate filter combiner null-value term (next a) next b))))


(define (prime-square-sum a b) ;a
  (filtered-accumulate prime? + 0 square a inc b))

(define (product-prime-to n) ;b
  (define (prime-to? x)
    (= (gcd x n) 1))
  (filtered-accumulate prime-to? * 1 identity 1 inc n))

(define (square x) (* x x))

(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))